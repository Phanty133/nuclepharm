﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelEndScreen : MonoBehaviour
{
    public static Dictionary<string, string> gameOverMessage = new Dictionary<string, string>()
    {
        { "time", "The workday has ended! You ran out of time!"},
        { "deaths", "Too many patients have died!"}
    };

    private GameObject levelFinishedContainer;
    private GameObject levelFailedContainer;

    private void openMenu(bool levelWin, string gameOverType, Dictionary<string, int> levelScore) {
        gameObject.SetActive(true);

        if (levelFinishedContainer == null) levelFinishedContainer = gameObject.transform.Find("WinScreen").gameObject;
        if (levelFailedContainer == null) levelFailedContainer = gameObject.transform.Find("LoseScreen").gameObject;

        if (levelWin)
        {
            showLevelFinished(levelScore);
        }
        else {
            showGameOver(gameOverType);
        }
    }

    private void showGameOver(string gameOverType)
    {
        levelFinishedContainer.SetActive(false);
        levelFailedContainer.SetActive(true);

        //Update title

        levelFailedContainer.transform.Find("Title").GetComponent<Text>().text = $"Day {LevelManager.curLevel} failed!";

        //Update content

        levelFailedContainer.transform.Find("LoseMessage").GetComponent<Text>().text = gameOverMessage[gameOverType];
    }

    private void showLevelFinished(Dictionary<string, int> levelScore)
    {
        levelFailedContainer.SetActive(false);
        levelFinishedContainer.SetActive(true);

        //Update title

        levelFinishedContainer.transform.Find("Title").GetComponent<Text>().text = $"Day {LevelManager.curLevel} finished!";

        //Update content

        Transform scoreContainer = levelFinishedContainer.transform.Find("Scores");

        scoreContainer.Find("Overall").GetComponent<Text>().text = $"Score: {levelScore["overall"]}";
        scoreContainer.Find("Efficiency").GetComponent<Text>().text = $"Efficiency: {levelScore["efficiency"]}";
        scoreContainer.Find("Time").GetComponent<Text>().text = $"Time: {levelScore["time"]}";
        scoreContainer.Find("Mortality").GetComponent<Text>().text = $"Mortality: {levelScore["mortality"]}";
    }

    private void OnEnable()
    {
        Debug.Log("Level end start");

        LevelManager.OnEndDay += openMenu;
    }

    private void OnDisable()
    {
        LevelManager.OnEndDay -= openMenu;
    }
}
