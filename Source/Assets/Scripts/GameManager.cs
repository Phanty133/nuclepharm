﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int ingredientsPerUnlock = 4;
    public int startingDifficulty = 1;

    LevelManager levelManager;

    public GameObject startScreen;
    private static bool gameHadStarted = false;

    public GameObject nextDayOverlayTemplate;

    public int nextDayOverlayFadeDelay = 3; //Fade delay in seconds
    public int nextDayOverlayFadeLength = 5; //Fade length in seconds

    private bool nextDayOverlayOpen = false;
    private GameObject nextDayOverlay;
    private CanvasGroup nextDayOverlayImg;

    private float fadeTimer = 0;

    public delegate void StartClose();
    public static event StartClose OnStartClose;

    void generateNewClients() {
        for (int i = 0; i < LevelManager.difficulty; i++) { //Create clients depending on the difficulty
            ClientManager.clients.addClient(new ClientManager.Client());
        }
    }

    private void NewDayInit() {
        //Debug.Log("Setting up a new day");

        openNextDayOverlay();
        InventoryManager.inventory.clearMedicineItems();

        LevelManager.checkForIngredientUnlocks(); //Check if new ingredients are to be unlocked

        generateNewClients(); //Generate new clients

        LevelManager.giveMedicineShipment(); //Give new ingredients

        LevelManager.totalPatientCount = ClientManager.clients.Count;
        LevelManager.patientDeathThreshold = Mathf.FloorToInt(ClientManager.clients.Count * levelManager.patientDeathThresholdPercentage);

        openStartScreen();
    }

    private void OnClientDeath(int id) {
        if (LevelManager.patientDeaths > LevelManager.patientDeathThreshold) { //End the day, if the patient deaths exceed the threshold
            levelManager.endCurDay();
        }
    }

    private void closeAllMenus() {
        //Close any open menus

        GameObject menuOverlay = GameObject.Find("MenuOverlay");

        if (menuOverlay!=null)
        {
            if(menuOverlay.activeSelf) menuOverlay.GetComponent<MenuManager>().closeMenu();
        }

        GameObject.Find("DayEndOverlay").SetActive(false);
    }

    private void openNextDayOverlay() {
        nextDayOverlayOpen = true;

        nextDayOverlay = Instantiate(nextDayOverlayTemplate, GameObject.Find("Canvas").transform);

        nextDayOverlay.transform.Find("Text").GetComponent<Text>().text = $"Day {LevelManager.curLevel}";

        nextDayOverlayImg = nextDayOverlay.GetComponent<CanvasGroup>();
    }

    public void winScreenContinue()
    {
        closeAllMenus();

        levelManager.startNewDay();
        NewDayInit();
    }

    public void loseScreenContinue()
    {
        closeAllMenus();

        ClientManager.clients.removeAllClients();

        levelManager.rewindDay();
    }

    public void OnDayRewind() {
        levelManager.startNewDay();

        NewDayInit();
    }

    public void openStartScreen() {
        startScreen.SetActive(true);

        startScreen.transform.Find("DayTitle").GetComponent<Text>().text = $"Day {LevelManager.curLevel}";
        startScreen.transform.Find("PatientCount").GetComponent<Text>().text = $"Patient count: {LevelManager.totalPatientCount}";
        startScreen.transform.Find("PatientDeathThreshold").GetComponent<Text>().text = $"Allowed patient deaths: {LevelManager.patientDeathThreshold}";
        startScreen.transform.Find("Efficiency").GetComponent<Text>().text = $"Minimum amount of ingredients: {LevelManager.minPossibleIngredients}";
    }

    public void closeStartScreen() {
        startScreen.SetActive(false);

        OnStartClose?.Invoke();
    }

    public void resetGame(SaveSystem.SaveData saveData = null) { //If saveData entered, then reset game and load from save
        ClientManager.clients.removeAllClients(); //Remove any previously generated clients
        InventoryManager.inventory.clearInventory(); //Clear inventory

        CraftingManager.discoveredRecipes = new List<CraftingManager.Recipe>();
        LevelManager.ingredientProgression = new List<List<int>>(); //List for the progression of medicine deliveries
        LevelManager.levelScores = new List<Dictionary<string, int>>();

        gameStart(saveData);
    }

    private void gameStart(SaveSystem.SaveData saveData = null) {
        //InventoryInit.OnInventoryReady += OnInventoryReady;
        //ClientManager.OnManagerInit += OnClientManagerReady;
        //LevelManager.OnNewDay += OnNewDay;

        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        GameObject saveDataObj = GameObject.Find("SaveDataObject");
        SaveData saveDataComp = saveDataObj.GetComponent<SaveData>();

        if (saveDataComp.loadGame) {
            saveData = saveDataComp.saveData;

            InventoryManager.inventory = new InventoryManager.Inventory();

            CraftingManager.discoveredRecipes = new List<CraftingManager.Recipe>();
            LevelManager.ingredientProgression = new List<List<int>>(); //List for the progression of medicine deliveries
            LevelManager.levelScores = new List<Dictionary<string, int>>();

            ClientManager.clients = new ClientManager.ClientList();
        }

        if (saveData == null && !saveDataComp.loadGame)
        {
            Debug.Log("Not loading game");
            InventoryInit.init(); //Initialize inventory

            LevelManager.generateIngredientProgression(ingredientAmount: ingredientsPerUnlock);

            ClientManager.clients = new ClientManager.ClientList(); //Initialize client manager

            LevelManager.curLevel = 0;
            LevelManager.difficulty = startingDifficulty;

            levelManager.startNewDay();

            NewDayInit();
        }
        else {
            Debug.Log("load game");
            //Load game from SaveData

            LevelManager.ingredientProgression = saveData.ingredientProgression;

            //Add clients
            foreach (ClientManager.Client client in saveData.clientList) {
                ClientManager.clients.addClient(client);
            }

            //Level data

            LevelManager.curLevel = saveData.curLevel;
            LevelManager.difficulty = saveData.difficulty;

            LevelManager.ingredientsUsed = saveData.ingredientsUsed;
            LevelManager.minPossibleIngredients = saveData.minPossibleIngredients;
            LevelManager.totalIngredientCount = saveData.totalIngredients;

            LevelManager.curedPatients = saveData.curedPatients;
            LevelManager.totalPatientCount = saveData.totalPatients;
            LevelManager.patientDeaths = saveData.patientDeaths;
            LevelManager.patientDeathThreshold = saveData.patientDeathThreshold;

            LevelManager.curTime = saveData.curTime;

            GameObject.Find("ClockText").GetComponent<Text>().text = $"{LevelManager.curTime}:00\nDay {LevelManager.curLevel}";

            foreach (string jsonString in saveData.levelScores) {
                JSONObject json = new JSONObject(jsonString);
                Dictionary<string, int> scoreDict = new Dictionary<string, int>();

                scoreDict["overall"] = int.Parse(json.GetField("overall").ToString());
                scoreDict["efficiency"] = int.Parse(json.GetField("efficiency").ToString());
                scoreDict["time"] = int.Parse(json.GetField("time").ToString());
                scoreDict["mortality"] = int.Parse(json.GetField("mortality").ToString());

                LevelManager.levelScores.Add(scoreDict);
            }

            //Inventory data

            foreach (InventoryManager.Item item in saveData.inventoryItems) {
                InventoryManager.inventory.addItem(item);
            }

            InventoryRender.renderItems();

            //Recipe data

            CraftingManager.discoveredRecipes = saveData.discoveredRecipes;
            CraftingManager.recipeList = saveData.recipes;
        }

        //Load options

        SaveSystem.SaveOptions options = SaveSystem.LoadGameOptions();

        if (options != null) {
            TutorialManager.showTutorialTooltip = options.showTutorialTooltip;

            if (!TutorialManager.showTutorialTooltip) {
                GameObject.Find("TutorialManager").GetComponent<TutorialManager>().closeTooltipConfirm();
            }
        }

        //InventoryManager.inventory.printInventory();

        //If the client list is empty, end the current day
        ClientManager.OnClientsEmpty += levelManager.endCurDay;

        //If a client dies, check if it exceeds the threshold
        ClientManager.OnClientDied += OnClientDeath;

        LevelManager.OnRestartDay += OnDayRewind;

        gameHadStarted = true;
    }

    private void Start()
    {
        if (gameHadStarted)
        {
            resetGame();
        }
        else {
            gameStart();
        }
    }

    private void Update()
    {
        if (nextDayOverlayOpen) {
            fadeTimer += Time.deltaTime;

            nextDayOverlayImg.alpha = Mathf.Lerp(1, 0, (fadeTimer - nextDayOverlayFadeDelay) / nextDayOverlayFadeLength);

            if (nextDayOverlayImg.alpha <= 0) {
                Destroy(nextDayOverlay);
                fadeTimer = 0;
                nextDayOverlayOpen = false;
            }
        }   
    }
}
