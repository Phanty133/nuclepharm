﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ClientManager;
using static InventoryManager;

public class MainDebug : MonoBehaviour
{
    private ClientManager ClientManager;
    //private InventoryManager invComp;
    //private GameObject invObj;

    void runDebug() {
        //invComp = GameObject.Find("InventoryManager").GetComponent<InventoryManager>(); //Include inventory tracker component from playerstats
        //invObj = GameObject.FindGameObjectsWithTag("PlayerInventory");
        
        /*
        //Define array with the id's of wanted properties
        int[] newPropertyIDs = { 0, 3 };

        //Create a new PropertyList object
        PropertyList itemProperties = new PropertyList(newPropertyIDs);

        //Create a new Item object; 
        Item newItem = new Item(
            itemID: -1, // The id of the item, if it's -1, generate new ID
            initProperties: itemProperties, // The properties of the item
            baseIngredient: false, // Is the item a base ingredient; 
            startLocked: false, // Is the item locked at the start (WIP);
            initCount: 5 // The initial amount of the newly created item
        );

        //Add the newly created item to inventory
        inventory.addItem(newItem);

        inventory.setAmountItemSlot(0, 69);

        int[] otherItemPropIDs = { 1, 2 };

        PropertyList otherItemProperties = new PropertyList(otherItemPropIDs);

        Item otherItem = new Item(
            itemID: -1, // The id of the item, if it's -1, generate new ID
            initProperties: otherItemProperties, // The properties of the item
            baseIngredient: false, // Is the item a base ingredient; 
            startLocked: false, // Is the item locked at the start;
            initCount: 420 // The initial amount of the newly created item
        );

        inventory.addItem(otherItem);

        inventory.printInventory(); // Print the inventory to console

        InventoryRender.renderItems(); // Render the inventory items

        //InventoryRender.renderItem(0, parent: GameObject.Find("CrafterSlotA"), inCraftSlot: true);

        //Debug.Log(otherItem.properties);*/

        LevelManager.generateIngredientProgression(ingredientAmount: 8);

        Debug.Log("Ingredient unlock progression: ");

        string output = "{";

        foreach (List<int> itemGroup in LevelManager.ingredientProgression){
            output += "{";
            foreach (int item in itemGroup) {
                output += item + ", ";
            }

            output += "}, \n";
        }

        Debug.Log(output + "}");

        LevelManager.checkForIngredientUnlocks();

        clients.addClient(new Client());
        clients.addClient(new Client());
        clients.addClient(new Client());

        /*Debug.Log("Current patient symptoms: ");

        string symptomOutput = "{";

        foreach (Dictionary<string, int> symptom in currentPatientSymptoms) {
            symptomOutput += "{id: " + symptom["id"] + ", count: " + symptom["count"] + "},\n";
        }

        Debug.Log(symptomOutput + "}");*/

        LevelManager.giveMedicineShipment();
    }

    private void Start()
    {
        OnManagerInit += runDebug; //Run debug, once the client manager is initialized
    }
}
