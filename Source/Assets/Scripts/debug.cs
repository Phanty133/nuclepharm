﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static InventoryManager;
using static CraftingManager;

public class debug : MonoBehaviour
{
    //private InventoryManager.inventory invComp;
    private GameObject[] invObj;
    // Start is called before the first frame update
    void Start()
    {
        //invComp = GameObject.Find("PlayerStats").GetComponent<InventoryManager.inventory>(); //Include inventory tracker component from playerstats
        invObj = GameObject.FindGameObjectsWithTag("PlayerInventory");

        //Define array with the id's of wanted properties
        int[] newPropertyIDs = {0, 3};

        //Create a new PropertyList object
        PropertyList itemProperties = new PropertyList(newPropertyIDs);

        //Create a new Item object; 
        Item newItem = new Item(
            itemID:0, // The id of the item, if it's -1, generate new ID
            initProperties: itemProperties, // The properties of the item
            baseIngredient:true, // Is the item a base ingredient; 
            startLocked:false, // Is the item locked at the start (WIP);
            initCount: 5 // The initial amount of the newly created item
        );

        //Add the newly created item to inventory
        inventory.addItem(newItem);

        inventory.setAmountItemSlot(0, 69);

        int[] otherItemPropIDs = { 1, 2 };

        PropertyList otherItemProperties = new PropertyList(otherItemPropIDs);

        Item otherItem = new Item(
            itemID: -1, // The id of the item, if it's -1, generate new ID
            initProperties: otherItemProperties, // The properties of the item
            baseIngredient: false, // Is the item a base ingredient; 
            startLocked: false, // Is the item locked at the start;
            initCount: 420 // The initial amount of the newly created item
        );

        inventory.addItem(otherItem);

        //InventoryManager.inventory.printInventory(); // Print the inventory to console

        InventoryRender.renderItems(); // Render the inventory items

        //InventoryRender.renderItem(0, parent: GameObject.Find("CrafterSlotA"), inCraftSlot: true);

        //Debug.Log(otherItem.properties);

        //   CLIENT DEBUG //

        ClientManager.Client testClient = new ClientManager.Client();

        ClientManager.clients.addClient(testClient);
        ClientManager.clients.printClients();

        inventory.printUnlockedProperties();

        Item itemA = inventory.getItemBySlot(0);
        Item itemB = inventory.getItemBySlot(1);

        int[] resultItemPropIDs = {4, 5};

        PropertyList resultItemProperties = new PropertyList(resultItemPropIDs);

        Item itemResult = new Item(
            itemID: -1, // The id of the item, if it's -1, generate new ID
            initProperties: resultItemProperties, // The properties of the item
            baseIngredient: false, // Is the item a base ingredient; 
            startLocked: false, // Is the item locked at the start;
            initCount: 1337 // The initial amount of the newly created item
        );

        Recipe recipeA = new Recipe(itemA, itemB, itemResult); //Ingredient A, ingredientB, result item
        Recipe recipeB = new Recipe(itemB, itemResult, itemA);

        //Add the recipes to global recipe list
        recipeList.Add(recipeA);
        recipeList.Add(recipeB);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
