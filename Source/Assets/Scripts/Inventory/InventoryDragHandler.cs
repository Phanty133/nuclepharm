﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    private LayoutElement layEl;
    private int originalSlot;
    //public int curSlot;
    private Transform selectObj;
    private Transform invParent;

    public bool inSlot = false;

    public delegate void ItemDropped();
    public static event ItemDropped OnItemDrop; //Invoked, when an item has been dropped

    static public List<GameObject> findObjectsWithTagByParent(string tag, GameObject parent) {
        List<GameObject> output = new List<GameObject>();
        for (int index = 0; index < parent.transform.childCount; index++) {
            GameObject child = parent.transform.GetChild(index).gameObject;
            if (child.tag == tag) output.Add(child);
        }

        return output;
    }

    static public bool vector2OverObject(Vector2 pos, GameObject obj) {
        Transform objT = obj.transform;
        Vector2 objPos = objT.position;
        Vector2 objSize = objT.GetComponent<RectTransform>().sizeDelta;
        Vector2 topLeft = objPos - objSize;
        Vector2 bottomRight = objPos + objSize;

        return pos.x >= topLeft.x && pos.y >= topLeft.y && pos.x <= bottomRight.x && pos.y <= bottomRight.y;
    }

    public void OnBeginDrag(PointerEventData eventData) {
        if (!gameObject.GetComponent<InventoryItemData>().itemData.locked) {
            layEl.ignoreLayout = true; //Remove item from grid
            originalSlot = gameObject.transform.GetSiblingIndex();
            //invParent = gameObject.transform.parent;
            //gameObject.transform.SetAsLastSibling(); //Make it render on top of everything
            //gameObject.transform.SetParent(selectObj);

            gameObject.transform.SetParent(selectObj);
            inSlot = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(!gameObject.GetComponent<InventoryItemData>().itemData.locked) gameObject.transform.position = Input.mousePosition;
    }
    
    public void OnEndDrag(PointerEventData eventData) {
        if (!gameObject.GetComponent<InventoryItemData>().itemData.locked) {
            if (inSlot) //If item in craft slot
            {
                if (vector2OverObject(eventData.position, gameObject.transform.parent.gameObject))
                { //Check if object not over set crafting slot
                    inSlot = false;
                }
            }
            else
            { //If item wasn't in a craft slot
                foreach (GameObject obj in GameObject.FindGameObjectsWithTag("CraftSlot"))
                {
                    if (vector2OverObject(eventData.position, obj) && obj.transform.childCount == 0) //Also check if the crafting slot has an item in it
                    {
                        inSlot = true;
                        gameObject.transform.position = obj.transform.position;
                        gameObject.transform.SetParent(obj.transform);
                    }
                }

                if (!inSlot) { //If item still isnt in a slot
                    foreach (GameObject obj in GameObject.FindGameObjectsWithTag("ItemSlot")) {
                        if (vector2OverObject(eventData.position, obj) && obj.transform.childCount == 0) //Also check if the crafting slot has an item in it
                        {
                            inSlot = true;
                            gameObject.transform.position = obj.transform.position;
                            gameObject.transform.SetParent(obj.transform);
                        }
                    }
                }
            }

            if (!inSlot)
            { //If object still not in crafting slot
                Transform invParent = GameObject.Find("InvContainer").transform;
                gameObject.transform.SetParent(invParent);
                gameObject.transform.SetSiblingIndex(originalSlot);
                layEl.ignoreLayout = false;
            }

            OnItemDrop?.Invoke();
        }
    }

    void Start() {
        layEl = gameObject.GetComponent<LayoutElement>();
        selectObj = GameObject.Find("CurItemSelection").transform;
    }
}
