﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static InventoryManager;
using static CraftingManager;

public class InventoryInit : MonoBehaviour
{
    //private InventoryManager invComp;
    //private GameObject[] invObj;

    //public delegate void inventoryReady();
    //public static event inventoryReady OnInventoryReady; //Define OnNewDay event, which is invoked, when day has started

    public static void init() {
        /*
        Hardcoded item list:
            Ibuprofenum
            Activated Carbon
            Salt
            Diosmectitum
            Levomenthol
            Amylmethacresol
            Citric acid
            Paracetamol
            Water
        */

        List<string> nameList = new List<string>()
        {
            "Ibuprofenum",
            "Activated Carbon",
            "Salt",
            "Diosmectitum",
            "Levomenthol",
            "Amylmethacresol",
            "Citric acid",
            "Paracetamol",
            "Water"
        };

        Item.nameList.AddRange(nameList); //Add the hardcoded item names

        Item.lastGlobalItemID = nameList.Count - 1; //Update global item ID

        Item itemIbuprofenum = new Item(
            itemID: 0, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: true, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Pharmaceutical drug, very effective at pain reduction." //The item description
        );

        inventory.addItem(itemIbuprofenum);

        Item itemCarbon = new Item(
            itemID: 1, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: true, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Highly adsorbent powdered or granular carbon." //The item description
        );

        inventory.addItem(itemCarbon);

        Item itemSalt = new Item(
            itemID: 2, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: true, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Abundant in nature, and is used primarily to season or preserve food." //The item description
        );

        inventory.addItem(itemSalt);

        Item itemDio = new Item(
            itemID: 3, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: true, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Highly effective at treating diarrhea." //The item description
        );

        inventory.addItem(itemDio);

        Item itemLevo = new Item(
            itemID: 4, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: true, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Organic compound made synthetically or obtained from peppermint." //The item description
        );

        inventory.addItem(itemLevo);

        Item itemAmyl = new Item(
            itemID: 5, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: true, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Great for bacterial and virus infection on and inside the body." //The item description
        );

        inventory.addItem(itemAmyl);

        Item itemCitric = new Item(
            itemID: 6, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: true, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Present in practically all plants and in many animal tissues and fluids." //The item description
        );

        inventory.addItem(itemCitric);

        Item itemParacetamol = new Item(
            itemID: 7, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: true, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Pharmaceutical drug, effective at treating mild-pain and dizziness." //The item description
        );

        inventory.addItem(itemParacetamol);

        Item itemWater = new Item(
            itemID: 8, // The id of the item, if it's -1, generate new ID
            baseIngredient: true, // Is the item a base ingredient; 
            startLocked: false, // Is the item locked at the start;
            initCount: 0, // The initial amount of the newly created item
            initDescription: "Colorless and odorless substance found all over Earth." //The item description
        );

        inventory.addItem(itemWater);

        InventoryRender.renderItems();

        //--------------------------------Initialize the hardcoded recipes--------------------------------

        //Define the reusable variables

        PropertyList itemProperties;
        Item itemResult;

        //-------Initialize the recipes-------

        //Initialize water based recipes

        itemProperties = new PropertyList(new List<int>() { 1 }); //Cough
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemSalt, itemWater, itemResult));

        itemProperties = new PropertyList(new List<int>() { 12 }); //Pain
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemIbuprofenum, itemWater, itemResult));

        itemProperties = new PropertyList(new List<int>() { 4 }); //Numbness
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemCitric, itemWater, itemResult));

        itemProperties = new PropertyList(new List<int>() { 0 }); //Constipation
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemCarbon, itemWater, itemResult));

        //Other recipes

        itemProperties = new PropertyList(new List<int>() { 1, 6 }); //Cough, Nasal congestion 
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemCitric, itemSalt, itemResult));

        itemProperties = new PropertyList(new List<int>() { 1, 12 }); //Cough, Pain
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemAmyl, itemIbuprofenum, itemResult));

        itemProperties = new PropertyList(new List<int>() { 2, 5 }); //Diarrhea, Vomiting
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemDio, itemCitric, itemResult));

        itemProperties = new PropertyList(new List<int>() { 7, 9 }); //Headache, fever
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemParacetamol, itemIbuprofenum, itemResult));

        itemProperties = new PropertyList(new List<int>() { 2, 0 }); //Diarrhea, Constipation
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemCarbon, itemDio, itemResult));

        itemProperties = new PropertyList(new List<int>() { 3, 8 }); //Sore throat, difficulty swallowing
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemLevo, itemAmyl, itemResult));

        itemProperties = new PropertyList(new List<int>() { 2, 5 }); //Dizziness, vomiting
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemParacetamol, itemDio, itemResult));

        itemProperties = new PropertyList(new List<int>() { 4, 7 }); //Numbness, headache
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemIbuprofenum, itemCitric, itemResult));

        itemProperties = new PropertyList(new List<int>() { 10 }); //Confusion
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemAmyl, itemParacetamol, itemResult));

        itemProperties = new PropertyList(new List<int>() { 11, 5 }); //Infection, vomiting
        itemResult = new Item(initProperties: itemProperties);

        recipeList.Add(new Recipe(itemLevo, itemCarbon, itemResult));

        //OnInventoryReady?.Invoke(); //Invoke inventory ready event
    }
}
