﻿using UnityEngine;
using UnityEngine.UI;

public class ClientRenderer : MonoBehaviour
{
    public static GameObject renderClient(ClientManager.Client client) {
        //GameObject clientObj = Instantiate(clientTemplate, transform);

        //client.gameObject = clientObj;

        GameObject clientObj = GameObject.Find("ClientContainer");

        //Transform itemSprite = clientObj.transform.GetChild(0);
        Transform clientName = clientObj.transform.Find("ClientName");
        Transform clientSymptoms = clientObj.transform.Find("SymptomContainer").Find("Content");
        Transform clientAvoid = clientObj.transform.Find("AvoidContainer").Find("Content");

        //clientObj.GetComponent<ClientData>().data = client;
        clientName.GetComponent<Text>().text = client.name;
        clientSymptoms.GetComponent<Text>().text = client.symptoms.ToStringList();
        clientAvoid.GetComponent<Text>().text = client.harmfulProperties.ToStringList();

        if (!ClientManager.clientColor.ContainsKey(client.id))
        {
            ClientManager.clientColor[client.id] = Color.HSVToRGB(Random.Range(0f, 1f), 0.35f, 0.75f);
        }

        clientObj.transform.Find("PatientImgFrame").Find("PatientImg").GetComponent<Image>().color = ClientManager.clientColor[client.id];

        //Add give medicine listener

        Button giveMedicineBtn = clientObj.transform.Find("GiveMedicineContainer").Find("GiveMedicineBtn").GetComponent<Button>();

        giveMedicineBtn.onClick.RemoveAllListeners(); //Remove any previous listener
        giveMedicineBtn.onClick.AddListener(client.giveMedicine); //Add current client's give medicine function

        //Update patient switching text

        clientObj.transform.Find("PatientSelect").Find("PatientNum").GetComponent<Text>().text = $"{MenuManager.curClientSelected + 1} / {ClientManager.clients.Count}";

        return clientObj; 
    }

    public static void removeCurrentClient() {
        if (MenuManager.curClientSelected == ClientManager.clients.Count) { //If it's the last client in the list, select the previous one
            MenuManager.curClientSelected--;
        }

        //Place the medicine back into inventory, if there is an item left

        Transform medicineSlot = GameObject.Find("ClientContainer").transform.Find("MedicineSlot");

        if (medicineSlot.childCount > 0)
        {
            Transform medicineSlotChild = medicineSlot.GetChild(0);

            Destroy(medicineSlotChild.gameObject);
        }

        if (MenuManager.curClientSelected < 0) {
            //GameObject.Find("ClientContainer").SetActive(false);
        }
        else {
            //Debug.Log(MenuManager.curClientSelected);

            renderClient(ClientManager.clients.getClient(MenuManager.curClientSelected)); //Rerender the new reordered client   

            MenuManager.checkPatientSwitchBtn();
        }
    }
}
