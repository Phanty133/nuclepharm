﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSwitch : MonoBehaviour
{
    public GameObject craftingMenu;
    public GameObject clientMenu;

    public GameObject craftingMenuBtn;
    public GameObject clientMenuBtn;

    public GameObject activeMenu;

    private Button craftingBtn;
    private Button clientBtn;

    public void openCraftingMenu() {
        ColorBlock craftingBtnColors = craftingBtn.colors;
        craftingBtnColors.colorMultiplier = 2f;
        craftingBtn.colors = craftingBtnColors;

        ColorBlock clientBtnColors = clientBtn.colors;
        clientBtnColors.colorMultiplier = 1f;
        clientBtn.colors = clientBtnColors;

        clientMenu.SetActive(false);
        craftingMenu.SetActive(true);

        activeMenu = craftingMenu;
    }

    public void openClientMenu() {
        ColorBlock craftingBtnColors = craftingBtn.colors;
        craftingBtnColors.colorMultiplier = 1f;
        craftingBtn.colors = craftingBtnColors;

        ColorBlock clientBtnColors = clientBtn.colors;
        clientBtnColors.colorMultiplier = 2f;
        clientBtn.colors = clientBtnColors;

        craftingMenu.SetActive(false);
        clientMenu.SetActive(true);

        activeMenu = clientMenu;
    }

    void Start()
    {
        craftingBtn = craftingMenuBtn.GetComponent<Button>();
        clientBtn = clientMenuBtn.GetComponent<Button>();

        if (activeMenu != null) {
            if (activeMenu == craftingMenu)
            {
                openCraftingMenu();
            }
            else
            {
                openClientMenu();
            }
        }
    }
}
