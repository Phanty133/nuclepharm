﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;

    public delegate void menuOpen();
    public static event menuOpen OnMenuOpen;

    public delegate void menuClose();
    public static event menuClose OnMenuClose;

    public delegate void buttonClick();
    public static event buttonClick OnButtonClick;

    public void open() {
        pauseMenu.SetActive(true);
        OnMenuOpen?.Invoke();
    }

    public void close() {
        pauseMenu.SetActive(false);
        OnMenuClose?.Invoke();
    }

    public void saveGame() {
        OnButtonClick?.Invoke();
        SaveSystem.SaveGame("test1");
    }

    public void loadGame() {
        OnButtonClick?.Invoke();
        GameObject.Find("GameManager").GetComponent<GameManager>().resetGame(SaveSystem.LoadGame("test1"));
    }

    public void returnToMain() {
        Destroy(GameObject.Find("SaveDataObject"));
        SceneManager.LoadScene("MainMenu");
        OnButtonClick?.Invoke();
    }

    public void quitGame() {
        Application.Quit();
        OnButtonClick?.Invoke();
    }

    public void openTutorial() {
        close();
        GameObject.Find("TutorialManager").GetComponent<TutorialManager>().openTutorial();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (pauseMenu.activeSelf)
            {
                close();
            }
            else {
                open();
            }
        }
    }
}
