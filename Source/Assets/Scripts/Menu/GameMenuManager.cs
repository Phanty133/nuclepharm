﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuManager : MonoBehaviour
{
    public void StartNewGame() {
        playAudio(menuClick);
        SceneManager.LoadScene("Main");
    }

    public void ExitGame() {
        playAudio(menuClose);
        Application.Quit();
    }

    public void OpenLoadGame() {
        playAudio(menuClick);

        SaveSystem.SaveData data = SaveSystem.LoadGame("test1");

        if (data != null) {
            SaveData dataComp = GameObject.Find("SaveDataObject").GetComponent<SaveData>();
            dataComp.saveData = data;
            dataComp.loadGame = true;

            SceneManager.LoadScene("Main");
        }
    }

    //Audio

    public AudioClip menuClick;
    public AudioClip menuOpen;
    public AudioClip menuClose;

    private AudioSource source;

    private void playAudio(AudioClip audio) {
        if (source == null) source = GameObject.Find("Audio").GetComponent<AudioSource>();

        source.clip = audio;
        source.Play();
    }

    private void Start()
    {
        DontDestroyOnLoad(GameObject.Find("Audio"));
        source = GameObject.Find("Audio").GetComponent<AudioSource>();
    }
}
