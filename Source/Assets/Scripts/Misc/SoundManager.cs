﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] BackgroundMusic;
    public AudioClip MainMenuBG;

    public AudioClip craftingSound;
    public AudioClip itemDrop;
    public AudioClip menuClick;
    public AudioClip menuOpen;
    public AudioClip menuClose;
    public AudioClip pageTurn;
    public AudioClip patientCure;
    public AudioClip patientDeath;
    public AudioClip patientSwitch;
    public AudioClip pauseOpen;
    public AudioClip pauseClose;
    public AudioClip craftingFail;

    private AudioSource source;
    private AudioSource backgroundSource;

    void playGameBackgroundMusic() {
        new WaitForSeconds(Random.Range(0, 5));

        backgroundSource.clip = BackgroundMusic[Random.Range(0, BackgroundMusic.Length)];

        backgroundSource.Play();
    }

    void playAudio(AudioClip audio) {
        if (source == null) source = GameObject.Find("Audio").GetComponent<AudioSource>();

        source.clip = audio;
        source.Play();
    }

    void stopAudio() {
        source.Stop();
    }

    void OnPatientCure(int id) {
        playAudio(patientCure);
    }

    void OnPatientDeath(int id) {
        playAudio(patientDeath);
    }

    void OnItemDrop() {
        playAudio(itemDrop);
    }

    void OnCraftingStart() {
        playAudio(craftingSound);
    }

    void OnCraftingStop() {
        stopAudio();
    }

    void OnPageTurn() {
        playAudio(pageTurn);
    }

    void OnMenuClick() {
        playAudio(menuClick);
    }

    void OnMenuOpen() {
        playAudio(menuOpen);
    }

    void OnMenuClose() {
        playAudio(menuClose);
    }

    void OnClientSwitch() {
        playAudio(patientSwitch);
    }

    void OnPauseOpen() {
        playAudio(pauseOpen);
    }

    void OnPauseClose()
    {
        playAudio(pauseClose);
    }

    void OnCraftingFail() {
        playAudio(craftingFail);
    }

    private void Update()
    {
        if (!backgroundSource.isPlaying) {
            playGameBackgroundMusic();
        }
    }

    void Start()
    {
        //if(source==null) source = Instantiate(new GameObject()).AddComponent<AudioSource>() as AudioSource;
        DontDestroyOnLoad(GameObject.Find("Audio"));
        source = GameObject.Find("Audio").GetComponent<AudioSource>();
        source.volume = 0.15f;
        backgroundSource = GameObject.Find("Main Camera").GetComponent<AudioSource>();

        //Patients
        ClientManager.OnClientCured += OnPatientCure;
        ClientManager.OnClientDied += OnPatientDeath;

        //Items

        InventoryDragHandler.OnItemDrop += OnItemDrop;

        //Menu

        CraftingManager.OnCraftingStart += OnCraftingStart;
        CraftingManager.OnCraftingEnd += OnCraftingStop;
        CraftingManager.OnCraftingFail += OnCraftingFail;

        MenuManager.OnMenuOpen += OnMenuOpen;
        MenuManager.OnMenuClose += OnMenuClose;
        MenuManager.OnClientSwitch += OnClientSwitch;

        PauseMenu.OnMenuOpen += OnPauseOpen;
        PauseMenu.OnMenuClose += OnPauseClose;
        PauseMenu.OnButtonClick += OnMenuClick;

        GameManager.OnStartClose += OnMenuClose;

        TutorialManager.OnMenuClick += OnMenuClick;
        TutorialManager.OnMenuClose += OnMenuClose;
        TutorialManager.OnMenuOpen += OnMenuOpen;
        TutorialManager.OnSlideChange += OnClientSwitch;
    }
}
