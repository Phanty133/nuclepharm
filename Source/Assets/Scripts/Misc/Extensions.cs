﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Extensions : MonoBehaviour
{
    public static bool ScrambledEquals<T>(IEnumerable<T> list1, IEnumerable<T> list2)
    {
        var cnt = new Dictionary<T, int>();
        foreach (T s in list1)
        {
            if (cnt.ContainsKey(s))
            {
                cnt[s]++;
            }
            else
            {
                cnt.Add(s, 1);
            }
        }
        foreach (T s in list2)
        {
            if (cnt.ContainsKey(s))
            {
                cnt[s]--;
            }
            else
            {
                return false;
            }
        }
        return cnt.Values.All(c => c == 0);
    }

    public static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }

    public static List<T> selectRandomElements<T>(List<T> inList, int neededCount){
        if (inList.Count == 0) return null;

        List<T> output = new List<T>();
        List<T> tempList = new List<T>();

        tempList.AddRange(inList);

        for (int i = 0; i < neededCount; i++) {
            int id = Random.Range(0, tempList.Count-1);
            output.Add(tempList[id]);
            tempList.RemoveAt(id);
        }

        return output;
    }

    public static string getListAsString<T>(List<T> inList) {
        string output = "{";

        for (int i = 0; i < inList.Count; i++) {
            output += inList[i] + (i == inList.Count - 1 ? "\n}" : ",\n");
        }

        return output;
    }
}
