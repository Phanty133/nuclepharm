﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SpriteHighlight : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Sprite highlightedSprite;

    private Sprite originalSprite;
    private Image imgComp;
    private RectTransform rect;
    private Vector2 originalSize;
    private Vector2 originalPos;

    public void OnPointerEnter(PointerEventData eventData)
    {
        imgComp.sprite = highlightedSprite;

        /*Vector2 spriteToScreenRatio = rect.sizeDelta / highlightedSprite.rect.size;

        rect.sizeDelta = originalSize + new Vector2(2f, 2f) * spriteToScreenRatio;
        rect.anchoredPosition = rect.anchoredPosition + (new Vector2(0.4f, 0.4f) * spriteToScreenRatio);*/
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        imgComp.sprite = originalSprite;
        //rect.sizeDelta = originalSize;
        //rect.anchoredPosition = originalPos;
    }

    // Start is called before the first frame update
    void Start()
    {
        imgComp = GetComponent<Image>();
        rect = GetComponent<RectTransform>();

        originalSprite = imgComp.sprite;
        originalSize = rect.sizeDelta;
        originalPos = rect.anchoredPosition;
    }
}
