﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartSceneManager : MonoBehaviour
{
    private string[] text = {
        "It was back then in 2020 when it happened. One thing was for sure and everybody knew it - peace was never an option. But nobody ever thought that a small incident like this would be the start of a war between nations...",
        "The year is 2050 and a lot has changed in the past 30 years. The population has gone down rapidly, people are getting sicker every day. We have been living in destroyed houses the past years until our group found a relatively undamaged city a few hundred kilometers away. ",
        "No one was living there. Our group moved to the city, but a lot of our people were ill, so I decided to open up a hospital, where everyone could come and get help. It was hard to gather some resources, since everything was scavenged already. ",
        "On an expedition to a nearby town, we stumbled upon a post-war hospital, where our group found some resources, which will be quite useful. ",
        "After about 10 days after moving to the city, the hospital could start work, but one day, a rival group attacked us, and took over the city. They didn’t have anyone, who knows medicine, so they trapped me in my own hospital, and forced me to work. I’ve been here ever since. "
    };

    public int logoFadeDelay = 2;
    public int logoFadeTime = 5;

    public int timePerSlide = 10;

    public Sprite[] img;

    private int curSlide = -1;

    private Image image;
    private Text caption;

    private Image logoImage;
    private bool logoShown;

    private float logoTimer;
    private float slideTimer;
    private float fadeTimer;

    private GameObject storyContainer;
    private bool slideFadeOut = false;

    private void showLogo() {
        logoShown = true;
    }

    private void nextSlide() {
        curSlide++;

        if (curSlide >= text.Length)
        { //If out of slides
            SceneManager.LoadScene("MainMenu");
        }

        image.sprite = img[curSlide];
        caption.text = text[curSlide];
    }

    private void Start()
    {
        image = GameObject.Find("Image").GetComponent<Image>();
        caption = GameObject.Find("Caption").GetComponent<Text>();
        logoImage = GameObject.Find("Logo").GetComponent<Image>();
        storyContainer = image.transform.parent.gameObject;

        storyContainer.SetActive(false);

        showLogo();
    }

    private void Update()
    {
        if (Input.anyKey) { //If any key pressed, skip story
            SceneManager.LoadScene("MainMenu");
        }

        if (logoShown)
        {
            logoTimer += Time.deltaTime;

            logoImage.color = new Color(1, 1, 1, Mathf.Lerp(1, 0, (logoTimer - logoFadeDelay) / logoFadeTime));

            if (logoImage.color.a <= 0) {
                logoShown = false;

                GameObject.Find("Logo").SetActive(false);
                storyContainer.SetActive(true);
                storyContainer.GetComponent<Animator>().SetTrigger("FadeIn");

                nextSlide();
            }
        }
        else if(!slideFadeOut){
            slideTimer += Time.deltaTime;

            if (slideTimer >= timePerSlide) {
                storyContainer.GetComponent<Animator>().SetTrigger("FadeOut");
                slideFadeOut = true;

                slideTimer = 0;
            }
        }

        if (slideFadeOut) {
            fadeTimer += Time.deltaTime;

            if (fadeTimer >= 1.7) { //If animation over
                nextSlide();
                slideFadeOut = false;
                fadeTimer = 0;
            }
        }
    }
}
