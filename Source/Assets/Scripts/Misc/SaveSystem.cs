﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    [System.Serializable]
    public class SaveData { //Save data class for progress data
        //Level data
        public int ingredientsUsed;
        public int minPossibleIngredients;
        public float totalIngredients;

        public int totalPatients;
        public float patientDeaths;
        public int curedPatients;
        public int patientDeathThreshold;

        public int curTime;

        public int curLevel;
        public int difficulty;

        public List<List<int>> ingredientProgression;
        public List<string> levelScores = new List<string>(); //Level scores in json format

        //Inventory data

        public List<InventoryManager.Item> inventoryItems;

        //Client data

        public List<ClientManager.Client> clientList;

        //Recipe data

        public List<CraftingManager.Recipe> discoveredRecipes;
        public List<CraftingManager.Recipe> recipes;

        public SaveData() {
            ingredientsUsed = LevelManager.ingredientsUsed;
            minPossibleIngredients = LevelManager.minPossibleIngredients;
            totalIngredients = LevelManager.totalIngredientCount;

            totalPatients = LevelManager.totalPatientCount;
            patientDeaths = LevelManager.patientDeaths;
            curedPatients = LevelManager.curedPatients;
            patientDeathThreshold = LevelManager.patientDeathThreshold;

            curTime = LevelManager.curTime;

            curLevel = LevelManager.curLevel;
            difficulty = LevelManager.difficulty;

            ingredientProgression = LevelManager.ingredientProgression;

            foreach (Dictionary<string, int> scoreDict in LevelManager.levelScores) {
                JSONObject score = new JSONObject(JSONObject.Type.OBJECT);

                score.AddField("overall", scoreDict["overall"]);
                score.AddField("efficiency", scoreDict["efficiency"]);
                score.AddField("time", scoreDict["time"]);
                score.AddField("mortality", scoreDict["mortality"]);

                levelScores.Add(score.ToString());
            }

            //Inventory data

            inventoryItems = InventoryManager.inventory.items;

            //Client data

            clientList = ClientManager.clients.clientList;

            //Recipe data

            discoveredRecipes = CraftingManager.discoveredRecipes;
            recipes = CraftingManager.recipeList;
        }
    }

    [System.Serializable]
    public class SaveOptions { //Save data class for options/settings
        public bool showTutorialTooltip;

        public SaveOptions() {
            showTutorialTooltip = TutorialManager.showTutorialTooltip;
        }
    }

    public static void SaveGame(string SaveName) {
        SaveData data = new SaveData(); //Save current progress data

        BinaryFormatter formatter = new BinaryFormatter();

        string path = Path.Combine(Application.persistentDataPath, SaveName+".save");
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static SaveData LoadGame(string SaveName) {
        string path = Path.Combine(Application.persistentDataPath, SaveName + ".save");

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SaveData data = formatter.Deserialize(stream) as SaveData;
            stream.Close();

            return data;
        }
        else {
            Debug.LogWarning("Save file doesn't exist in " + path);
            return null;
        }
    }

    public static void SaveGameOptions() {
        SaveOptions data = new SaveOptions(); //Save current progress data

        BinaryFormatter formatter = new BinaryFormatter();

        string path = Path.Combine(Application.persistentDataPath, "game.options");
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static SaveOptions LoadGameOptions()
    {
        string path = Path.Combine(Application.persistentDataPath, "game.options");

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SaveOptions data = formatter.Deserialize(stream) as SaveOptions;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogWarning("Save file doesn't exist in " + path);
            return null;
        }
    }
}
